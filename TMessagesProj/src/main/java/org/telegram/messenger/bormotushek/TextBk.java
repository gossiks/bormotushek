package org.telegram.messenger.bormotushek;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;

import com.rogerlemmonapps.captcha.Captcha;

import org.telegram.messenger.R;

import java.io.CharArrayWriter;
import java.util.ArrayList;
import java.util.Random;


/**
 * Created by gossiks on 10/23/16.
 */

public class TextBk extends Captcha {

    private final TextOptions mOptions;
    private float mWordLength;
    private String mWord;
    private ArrayList<Integer> mUsedColors;
    private Context mContext;

    public enum TextOptions {
        DEFAULT
    }

    public TextBk(String word, TextOptions opt, Context context) {
        this(0, 0, word, opt, context);
    }

    public TextBk(int width, int height, String word, TextOptions opt, Context context) {
        setHeight(height);
        setWidth(width);
        this.mOptions = opt;
        mUsedColors = new ArrayList<>();
        mWordLength = word.length();
        mWord = word;
        mContext = context;
    }

    @Override
    public Bitmap getImage() {
        if (image == null) {
            image = image();
        }
        return image;
    }

    @Override
    protected Bitmap image() {
        LinearGradient gradient = new LinearGradient(0, 0, getWidth() / this.mWordLength, getHeight() / 2, colorBk(), colorBk(), Shader.TileMode.MIRROR);
        Paint p = new Paint();
        p.setDither(true);
        p.setShader(gradient);
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        c.drawRect(0, 0, getWidth(), getHeight(), p);
        Paint tp = new Paint();
        tp.setDither(true);
        tp.setTextSize(getWidth() / getHeight() * 20);

        Random r = new Random(System.currentTimeMillis());
        CharArrayWriter cab = new CharArrayWriter();
        for (char singleLetter : mWord.toCharArray()) {
            switch (mOptions) {
                case DEFAULT:
                    cab.append(singleLetter);
                    break;
                default:
                    break;
            }
        }

        char[] data = mWord.toCharArray();
        for (int i = 0; i < data.length; i++) {
            this.x += (30 - (3 * data.length)) + (Math.abs(r.nextInt()) % (65 - (1.2 * data.length)));
            this.y = 50 + Math.abs(r.nextInt()) % 50;
            Canvas cc = new Canvas(bitmap);
            tp.setTextSkewX(r.nextFloat() - r.nextFloat());
            tp.setColor(mContext.getResources().getColor(R.color.hockeyapp_text_black));
            cc.drawText(data, i, 1, this.x, this.y, tp);
            tp.setTextSkewX(0);
        }
        return bitmap;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int w) {
        width = w;
    }

    public void setHeight(int h) {
        height = h;
    }

    public int colorBk() {
        Random r = new Random();

        int number;
        do {
            number = r.nextInt(9);
        } while (mUsedColors.contains(Integer.valueOf(number)));

        mUsedColors.add(Integer.valueOf(number));
        switch (number) {
            case 0:
                return -16777216;
            case 1:
                return -16776961;
            case 2:
                return -16711681;
            case 3:
                return -12303292;
            case 4:
                return -7829368;
            case 5:
                return -16711936;
            case 6:
                return -65281;
            case 7:
                return -65536;
            case 8:
                return -256;
            case 9:
                return -1;
            default:
                return -1;
        }
    }
}
